import pandas as pd
from pudb import set_trace
import numpy as np

data = {'col_1': [[3, 2], [2, 1], [1, 0], [0, -1], [-1, -2]],
        'col_2': ['a', 'b', 'c', 'd', 'e']}
df = pd.DataFrame.from_dict(data)
for i in range(df.shape[0]):
    print(df.loc[i]['col_1'], df.loc[i]['col_2'])


def average_df(row1, row2):
    data = {'col_1': [((np.asarray(row1['col_1']) +
                        np.asarray(row2['col_1'])) // 2).tolist()],
            'col_2': row1['col_2'] + row2['col_2']}
    print(data)
    return pd.DataFrame.from_dict(data)


def permute(df):
    permuted_df = pd.DataFrame(columns=['col_1', 'col_2'])
    start = 0
    end = df.shape[0]
    for i in range(start, end - 1):
        for j in range(i + 1, end):
            row = average_df(df.loc[i], df.loc[j])
            print(row)
            permuted_df = permuted_df.append(row, ignore_index=True)
        start += 1
    return permuted_df


permuted_df = permute(df)
print(permuted_df)
concated_df = pd.concat([df, permuted_df], ignore_index=True)
print(concated_df)

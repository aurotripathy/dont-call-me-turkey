import argparse
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from keras.callbacks import ReduceLROnPlateau

from keras.models import Model
from keras.layers import Dense, Dropout, Bidirectional, CuDNNGRU, \
    GlobalMaxPooling1D, GlobalAveragePooling1D, Input, \
    concatenate, BatchNormalization, \
    LSTM
from keras import regularizers
from keras.optimizers import Adam
import numpy as np
from pudb import set_trace

max_len = 10
feature_size = 128


def read_dataset():
    sns.set_style('whitegrid')

    df_train = pd.read_json('./data/train.json')
    df_test = pd.read_json('./data/test.json')

    print("Number of train sample : ", df_train.shape[0])
    print("Number of test sample : ", df_test.shape[0])

    # Headers are:
    # ['audio_embedding',
    # 'start_time_seconds_youtube_clip', 'end_time_seconds_youtube_clip',
    # 'is_turkey', 'vid_id']
    print(df_train.columns.values)
    print(type(df_train['audio_embedding'][0]))

    print(df_train.head())
    print(df_test.head())

    plt.figure(figsize=(12, 8))
    sns.countplot(df_train['is_turkey'])
    plt.savefig('distribution.png')

    print(df_train.info())
    print(df_test.info())
    return df_train, df_test


def average_df(row1, row2, is_turkey):
    """ Average the audio embedding"""
    data = {'audio_embedding': [((np.asarray(row1['audio_embedding']) +
                                  np.asarray(row2['audio_embedding'])) // 2)],
            'is_turkey': is_turkey,
            'start_time_seconds_youtube_clip': 0,
            'end_time_seconds_youtube_clip': 10,
            'vid_id': 'generated'}
    # print(data)
    return pd.DataFrame.from_dict(data)


def augment_dataset(df, is_turkey):
    columns = ['audio_embedding',
               'start_time_seconds_youtube_clip',
               'end_time_seconds_youtube_clip',
               'is_turkey',
               'vid_id']
    permuted_df = pd.DataFrame(columns=columns)
    start = 0
    # end = df.shape[0]
    end = 200
    for i in range(start, end - 1):
        for j in range(i + 1, end):
            set_trace()
            row = average_df(df.loc[i], df.loc[j], is_turkey)
            # print(row)
            permuted_df = permuted_df.append(row, ignore_index=True)
        start += 1
    return permuted_df


def prep_dataset(df_train, df_test):

    # Video Lengths
    # This will be used for padding
    df_train["length"] = df_train['audio_embedding'].apply(len)
    min_len = min(df_train["length"])
    max_len = max(df_train["length"])
    print('Minimum length', min_len)
    print('Maximum length', max_len)

    # use max_len to combinatorially imcrease the data samples
    df_is_turkey = df_train[(df_train['length'] == max_len) &
                            (df_train['is_turkey'] == 1)]
    df_is_turkey.reset_index(inplace=True)
    df_is_not_turkey = df_train[(df_train['length']
                                 == max_len) & (df_train['is_turkey'] == 0)]
    df_is_not_turkey.reset_index(inplace=True)

    df_aug_is_turkey = augment_dataset(df_is_turkey, is_turkey=1)
    df_aug_is_not_turkey = augment_dataset(df_is_not_turkey, is_turkey=0)

    df_train = pd.concat([df_train, df_aug_is_turkey, df_aug_is_not_turkey])

    # append augmented data
    plt.figure(figsize=(12, 8))
    plt.yscale('log')
    sns.countplot("length", hue="is_turkey", data=df_train)
    plt.savefig('video_lenghts.png')

    # We will pad our videos to 10.
    # Data for the network
    # We apply padding, for inputs to have the same length,
    # and then split to evaluate our model.

    X = pad_sequences(df_train['audio_embedding'],
                      maxlen=max_len, padding='post')
    X_test = pad_sequences(df_test['audio_embedding'],
                           maxlen=max_len, padding='post')

    y = df_train['is_turkey'].values

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2)

    print(f"Training on {X_train.shape[0]} texts")
    return X, y, X_train, y_train, X_val, y_val, X_test


def run_model(args):

    def build_GRU_model():
        print('Building model GRU')
        inp = Input(shape=(max_len, feature_size))
        x = BatchNormalization()(inp)
        x = Bidirectional(CuDNNGRU(256, return_sequences=True))(x)
        x = Bidirectional(CuDNNGRU(128, return_sequences=True))(x)
        avg_pool = GlobalAveragePooling1D()(x)
        max_pool = GlobalMaxPooling1D()(x)
        concat = concatenate([avg_pool, max_pool])
        concat = Dense(64, activation="relu")(concat)
        concat = Dropout(0.5)(concat)
        output = Dense(1, activation="sigmoid")(concat)
        model = Model(inputs=inp, outputs=output)
        model.compile(loss='binary_crossentropy', optimizer=Adam(
            lr=0.0001), metrics=['accuracy'])
        return model

    def build_LSTM_model():
        print('Building model LSTM')
        kernel_regularizer = regularizers.l2(
            0.001) if args.regularizer else None
        recurrent_regularizer = regularizers.l2(
            0.001) if args.regularizer else None
        inp = Input(shape=(max_len, feature_size))
        x = BatchNormalization()(inp)
        x = Bidirectional(LSTM(256, return_sequences=True,
                               kernel_regularizer=kernel_regularizer,
                               recurrent_regularizer=recurrent_regularizer))(x)
        x = Bidirectional(LSTM(128, return_sequences=True,
                               kernel_regularizer=kernel_regularizer,
                               recurrent_regularizer=recurrent_regularizer))(x)

        avg_pool = GlobalAveragePooling1D()(x)
        max_pool = GlobalMaxPooling1D()(x)
        concat = concatenate([avg_pool, max_pool])
        fc_layer = Dense(64, activation="relu")(concat)
        fc_layer = Dropout(0.2)(fc_layer)
        output = Dense(1, activation="sigmoid")(fc_layer)
        model = Model(inputs=inp, outputs=output)
        model.compile(loss='binary_crossentropy', optimizer=Adam(
            lr=0.0001), metrics=['accuracy'])
        return model

    # model = build_GRU_model()
    model = build_GRU_model() if args.model_type == 'GRU' else build_LSTM_model()

    model.summary()

    reduce_lr = ReduceLROnPlateau(
        monitor='val_acc', factor=0.1, patience=2, verbose=1, min_lr=1e-8)

    epochs = 20

    df_train, df_test = read_dataset()
    X, y, X_train, y_train, X_val, y_val, X_test = prep_dataset(
        df_train, df_test)
    history = model.fit(X_train, y_train, batch_size=256,
                        epochs=epochs, validation_data=[X_val, y_val],
                        callbacks=[reduce_lr], verbose=2)

    # Learning Curves

    plt.figure(figsize=(12, 8))
    sns.lineplot(range(1, epochs+1),
                 history.history['acc'], label='Train Accuracy')
    sns.lineplot(range(1, epochs+1),
                 history.history['val_acc'], label='Test Accuracy')
    plt.savefig('Learning-curve.png')

    ###############
    # Predictions #
    ###############

    y_pred_val = model.evaluate(X_val, y_val, verbose=1)
    print("Train accuracy : ", y_pred_val[1])

    ################
    # Submission   #
    ################
    model_final = build_GRU_model() if args.model_type == 'GRU' else build_LSTM_model()

    epochs_final = 20
    reduce_lr_final = ReduceLROnPlateau(
        monitor='acc', factor=0.1, patience=2, verbose=1, min_lr=1e-8)

    model_final.fit(X, y, epochs=epochs_final, batch_size=256,
                    verbose=2, callbacks=[reduce_lr_final])
    y_test = model_final.predict(X_test, verbose=1)

    submission = pd.DataFrame(
        {'vid_id': df_test['vid_id'].values, 'is_turkey': list(y_test.flatten())})

    print(submission.head())

    submission.to_csv("submission.csv", index=False)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model_type", help="LSTM or GRU",
                        type=str, required=True, choices=['GRU', 'LSTM'])
    parser.add_argument("-r", "--regularizer", help="regularizers true/false",
                        action='store_true', default=False)

    args = parser.parse_args()
    print(args)
    run_model(args)


if __name__ == '__main__':
    main()
